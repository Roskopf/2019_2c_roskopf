
Firmware para la asignatura Electrónica Programable
-----------

Firmware inicial para el desarrollo de drivers de aplicación y proyectos del cursado con la plataforma [EDU-CIAA](www.proyecto-ciaa.com.ar/) basada en LPC4337JBD144. 

 *  [Repositorio oficial del proyecto CIAA.](https://github.com/ciaa)



**Autores:** 

 *  Juan Manuel Reta (jmreta@ingenieria.uner.edu.ar)
 *  Eduardo Filomena (efilomena@ingenieria.uner.edu.ar)
 *  Juan Ignacio Cerrudo (jcerrudo@ingenieria.uner.edu.ar)
 *  Albano Peñalva (apenialva@ingeneiria.uner.edu.ar)
 *  Sebastián Mateos (smateos@ingeneiria.uner.edu.ar)
 *  Joaquin Furios (joaquinfurios@gmail.com)






